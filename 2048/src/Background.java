import java.awt.Color;
import java.awt.Graphics2D;

public class Background {
	int h,w;
	Color color;
public Background() {
	color = Color.BLUE;
	h = GamePanel.HEIGHT;
	w = GamePanel.WIDTH;
}
public void draw(Graphics2D g) {
	g.setColor(color);
	g.fillRect(0, 0, w, h);
}
}
