import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JPanel;

public class GamePanel extends JPanel implements Runnable{
	
	public static int WIDTH = 700;
	public static int HEIGHT = 700;
	
private Thread thread;
	
	private BufferedImage image;
	
	private Graphics2D g;
	
	private int FPS;
	private double millisToFPS;
	private long timerFPS;
	private int sleepTime;
	
	private Background bg;
	public static ArrayList<ArrayList<Fields>> fields;
	public static ArrayList<ArrayList<Number>> numbers;
	public static ArrayList<Number> fusing;
	
	public static boolean spawn = false;
	private boolean able = false;
	
	public GamePanel() {
		super();
		
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		setFocusable(true);
		requestFocus();
		
		addKeyListener(new Listeners());
		addMouseMotionListener(new Listeners());
		addMouseListener(new Listeners());
		
		bg = new Background();
		fields = new ArrayList<ArrayList<Fields>>();
		numbers = new ArrayList<ArrayList<Number>>();
		fusing = new ArrayList<Number>();
		
		for (int i = 0; i < 4; i++) {
			fields.add(new ArrayList<Fields>());
			numbers.add(new ArrayList<Number>());
			for (int a = 0; a < 4; a++) {
				fields.get(i).add(new Fields(50+i*150, 50+a*150, 150));
				numbers.get(i).add(null);
			}
		}
		
		for (int i = 0; i < 2; i++) {
			int a = (int) (Math.random()*4);
			int b = (int) (Math.random()*4);
			while (numbers.get(a).get(b) != null) {
				a = (int) (Math.random()*4);
				b = (int) (Math.random()*4);
			}
			int ran = (int) (Math.random()*50);
			if (ran >= 25) {
				ran = 4;
			}
			else {
				ran = 2;
			}
			numbers.get(a).set(b, new Number(50+a*150, 50 + b*150, 150, ran));
		}
	}

	public void start() {
		thread = new Thread(this);
		thread.start();
	}
	
	@Override
	public void run() {
		FPS = 30;
		millisToFPS = 1000/FPS;
		sleepTime = 0;
		
		image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		g = (Graphics2D) image.getGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		while (true) {
			timerFPS = System.nanoTime();
			gameUpdate();
			gameRender();
			gameDraw();
			timerFPS = (System.nanoTime() - timerFPS)/1000000;
			if (timerFPS < millisToFPS) {
				sleepTime = (int) (millisToFPS - timerFPS);
			}
			else {
				sleepTime = 1;
			}
			try {
				thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void gameUpdate() {
		able = true;
		boolean lost = true;
		for (int i = 0; i < numbers.size(); i++) {
			for (int a = 0; a < numbers.get(i).size(); a++) {
				if (numbers.get(i).get(a) != null) {
				numbers.get(i).get(a).update();
				if (numbers.get(i).get(a).getXdest() != 0 || numbers.get(i).get(a).getYdest() != 0) {
					able = false;
				}
				}
				else {
					lost = false;
				}
			}
		}
		
		for (int i = 0; i < fusing.size(); i++) {
			fusing.get(i).update();
			if (fusing.get(i).getXdest() == 0 && fusing.get(i).getYdest() == 0) {
				numbers.get(fusing.get(i).getX1()).get(fusing.get(i).getY1()).setValue(fusing.get(i).getValue()*2);
				fusing.remove(i);
				i--;
			}
			else {
				able = false;
			}
		}
		
		if (able && spawn) {
			able = false;
			spawn = false;
			int a = (int) (Math.random()*4);
			int b = (int) (Math.random()*4);
			while (numbers.get(a).get(b) != null) {
				a = (int) (Math.random()*4);
				b = (int) (Math.random()*4);
			}
			int ran = (int) (Math.random()*50);
			if (ran >= 25) {
				ran = 4;
			}
			else {
				ran = 2;
			}
			numbers.get(a).set(b, new Number(50+a*150, 50 + b*150, 150, ran));
		}
		
		
		
		for (int i = 0; i < numbers.size(); i++) {
			if (lost) {
			for (int a = 0; a < numbers.get(i).size()-1; a++) {
				if (numbers.get(i).get(a) != null) {
					if (i < numbers.size() -1) {
						if (numbers.get(i).get(a).getValue() == numbers.get(i+1).get(a).getValue()) {
							lost = false;
							break;
						}
					}
					
						if (numbers.get(i).get(a+1).getValue() == numbers.get(i).get(a).getValue()) {
							lost = false;
							break;
						}
					
				}
			}
			}
		}
		
		if (lost) {
			for (int i = 0; i < numbers.size(); i++) {
				for (int a = 0; a < numbers.size(); a++) {
					numbers.get(i).set(a, null);
				}
			}
			for (int i = 0; i < 2; i++) {
				int a = (int) (Math.random()*4);
				int b = (int) (Math.random()*4);
				while (numbers.get(a).get(b) != null) {
					a = (int) (Math.random()*4);
					b = (int) (Math.random()*4);
				}
				int ran = (int) (Math.random()*50);
				if (ran >= 25) {
					ran = 4;
				}
				else {
					ran = 2;
				}
				numbers.get(a).set(b, new Number(50+a*150, 50 + b*150, 150, ran));
			}
		}
	}
	
	public void gameRender() {
		bg.draw(g);
		for (int i = 0; i < fields.size(); i++) {
			for (int a = 0; a < fields.size(); a++) {
				fields.get(i).get(a).draw(g);
			}
		}
		for (int i = 0; i < numbers.size(); i++) {
			for (int a = 0; a < numbers.size(); a++) {
				if (numbers.get(i).get(a) != null) {
					numbers.get(i).get(a).draw(g);
				}
			}
		}
		for (int i = 0; i < fusing.size(); i++) {
			fusing.get(i).draw(g);
		}
	}
	
	private void gameDraw() {
		Graphics g2 = this.getGraphics();
		g2.drawImage(image, 0, 0, null);
		g2.dispose();
	}
	
	
	
	
}
