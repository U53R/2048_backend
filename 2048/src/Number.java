import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

public class Number {
private int x,y,s,value,speed,xdest,ydest,x1,y1;
private Color color;
public Number(int x, int y, int s, int value) {
	color = Color.YELLOW;
	this.x = x;
	this.y = y;
	this.s = s;
	this.value = value;
	speed = 50;
}
public void move(int a, int b) {
	xdest = a;
	
	ydest = b;
	
	System.out.println(xdest);
}

public void update() {
	if (xdest != 0) {
		x += speed * (xdest/Math.abs(xdest));
		xdest += speed * (xdest/Math.abs(xdest)*-1);
		
	}
	if (ydest != 0) {
		y += speed * (ydest/Math.abs(ydest));
		ydest += speed * (ydest/Math.abs(ydest)*-1);
		
	}
}
public int getValue() {
	return value;
}
public void setValue(int v) {
	this.value = v;
}
public void fuse(int a, int b) {
	x1 = a;
	y1 = b;
}
public int getX1() {
	return x1;
}
public int getY1() {
	return y1;
}

public int getXdest() {
	return xdest;
}
public int getYdest() {
	return ydest;
}
public void draw(Graphics2D g) {
	g.setColor(color);
	g.fillRect(x, y, s, s);
	g.setColor(Color.BLACK);
	g.setFont(new Font("Consolas", Font.BOLD, 40));
	long length = (int) g.getFontMetrics().getStringBounds(Integer.toString(value), g).getWidth();
	long size = (int) g.getFontMetrics().getStringBounds(Integer.toString(value), g).getHeight();
	g.drawString(Integer.toString(value), (int)(x + s/2-length/2), (int)(y+s/2+size/4));
	g.setStroke(new BasicStroke(3));
	g.setColor(Color.BLACK);
	g.drawRect(x, y, s, s);
}
}
