import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

public class Fields {
private int x,y,s;
private Color color;
public Fields(int x, int y, int s) {
	color = Color.GRAY;
	this.x = x;
	this.y = y;
	this.s = s;
}
public void draw(Graphics2D g) {
	g.setColor(color);
	g.fillRect(x, y, s, s);
	g.setStroke(new BasicStroke(3));
	g.setColor(Color.BLACK);
	g.drawRect(x, y, s, s);
}
}
