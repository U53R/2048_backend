import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class Listeners implements KeyListener, MouseListener, MouseMotionListener{
public static int x, y;
public static boolean legit = false;
	@Override
	public void mouseDragged(MouseEvent e) {
		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		x = e.getX();
		y = e.getY();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		x = e.getX() - x;
		y = e.getY() - y;
		
		if (Math.abs(x) > Math.abs(y)) {
		if (x > 0) {
			int missing = 0;
			int val = -1;
				for (int a = 0 ; a < 4; a++) {
					for (int i = GamePanel.numbers.size()-1; i > -1; i--) {
					if (GamePanel.numbers.get(i).get(a) != null) {
						if (val == GamePanel.numbers.get(i).get(a).getValue()) {
							missing++;
							GamePanel.numbers.get(i).get(a).move(150*missing, 0);
							
							GamePanel.numbers.get(i).get(a).fuse(i+missing, a);
							GamePanel.fusing.add(GamePanel.numbers.get(i).get(a));
							GamePanel.numbers.get(i).set(a, null);
							val=-1;
							legit = true;
						}
					else if (missing != 0) {
						GamePanel.numbers.get(i).get(a).move(150*missing, 0);
						
						GamePanel.numbers.get(i+missing).set(a, GamePanel.numbers.get(i).get(a));
						GamePanel.numbers.get(i).set(a, null);
						val = GamePanel.numbers.get(i+missing).get(a).getValue();
						legit = true;
					}
				else {
					val = GamePanel.numbers.get(i+missing).get(a).getValue();
				}
					}
					else {
						missing++;
					}
				}
				missing = 0;
				val = -1;
			}
				if (legit) {
				GamePanel.spawn = true;
				legit = false;
				}
		}
		else {
			int missing = 0;
			int val = -1;
				for (int a = 0 ; a < 4; a++) {
					for (int i = 0; i < GamePanel.numbers.get(a).size(); i++) {
					if (GamePanel.numbers.get(i).get(a) != null) {
						if (val == GamePanel.numbers.get(i).get(a).getValue()) {
							missing--;
							GamePanel.numbers.get(i).get(a).move(150*missing, 0);
							
							GamePanel.numbers.get(i).get(a).fuse(i+missing, a);
							GamePanel.fusing.add(GamePanel.numbers.get(i).get(a));
							GamePanel.numbers.get(i).set(a, null);
							val=-1;
							legit = true;
						}
					else if (missing != 0) {
						GamePanel.numbers.get(i).get(a).move(150*missing, 0);
						
						GamePanel.numbers.get(i+missing).set(a, GamePanel.numbers.get(i).get(a));
						GamePanel.numbers.get(i).set(a, null);
						val = GamePanel.numbers.get(i+missing).get(a).getValue();
						legit = true;
						}
					else {
						val = GamePanel.numbers.get(i+missing).get(a).getValue();
					}
						
					}
					else {
						missing--;
					}
				}
				missing = 0;
				val = -1;
			}
				if (legit) {
					GamePanel.spawn = true;
					legit = false;
					}
		}
		}
		else {
			if (y > 0) {
				int missing = 0;
				int val = -1;
					for (int a = 0 ; a < 4; a++) {
						for (int i = GamePanel.numbers.get(a).size()-1; i > -1; i--) {
						if (GamePanel.numbers.get(a).get(i) != null) {
							if (val == GamePanel.numbers.get(a).get(i).getValue()) {
								missing++;
								GamePanel.numbers.get(a).get(i).move(0, 150*missing);
								GamePanel.numbers.get(a).get(i).fuse(a, i+missing);
								GamePanel.fusing.add(GamePanel.numbers.get(a).get(i));
								GamePanel.numbers.get(a).set(i, null);
								val=-1;
								legit = true;
							}
						else if (missing != 0) {
							GamePanel.numbers.get(a).get(i).move(0, 150*missing);
							GamePanel.numbers.get(a).set(i+missing, GamePanel.numbers.get(a).get(i));
							GamePanel.numbers.get(a).set(i, null);
							val = GamePanel.numbers.get(a).get(i+missing).getValue();
							legit = true;
						}
					else {
						val = GamePanel.numbers.get(a).get(i+missing).getValue();
					}
						}
						else {
							missing++;
						}
					}
						val = -1;
					missing = 0;
				}
					if (legit) {
						GamePanel.spawn = true;
						legit = false;
						}
			}
			else {
				int missing = 0;
				int val = -1;
					for (int a = 0 ; a < 4; a++) {
						for (int i = 0; i < GamePanel.numbers.get(a).size(); i++) {
						if (GamePanel.numbers.get(a).get(i) != null) {
							if (val == GamePanel.numbers.get(a).get(i).getValue()) {
								missing--;
								GamePanel.numbers.get(a).get(i).move(0, 150*missing);
								GamePanel.numbers.get(a).get(i).fuse(a, i+missing);
								val=-1;
								GamePanel.fusing.add(GamePanel.numbers.get(a).get(i));
								GamePanel.numbers.get(a).set(i, null);
								legit = true;
							}
						else if (missing != 0) {
							GamePanel.numbers.get(a).get(i).move(0, 150*missing);
							GamePanel.numbers.get(a).set(i+missing, GamePanel.numbers.get(a).get(i));
							GamePanel.numbers.get(a).set(i, null);
							val = GamePanel.numbers.get(a).get(i+missing).getValue();
							legit = true;
						}
					else {
						val = GamePanel.numbers.get(a).get(i+missing).getValue();
					}
						}
						else {
							missing--;
						}
					}
						val = -1;
					missing = 0;
				}
					if (legit) {
						GamePanel.spawn = true;
						legit = false;
						}
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		
	}

	
	
}
